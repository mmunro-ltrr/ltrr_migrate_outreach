<?php
/**
 * @file
 * Copy the fields from Outreach Information into a UAQS Flexible Page.
 */

/**
 * Handle the final Outreach Information node migration.
 */
class LtrrMigrateOutreachChunksMigration extends DrupalNode7Migration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Documented lists of source data fields:.
    $fields = array(
      'body' => t('Body, for summary only'),
      'paragraph_items' => t('All content paragraphs'),
    );

    // The straight one-to-one mappings.
    $this->addSimpleMappings(array('title'));

    // Direct field mappings.
    $this->addFieldMapping('field_uaqs_summary', 'body:summary');

    // Paragraphs (UAQS Content Chunks).
    $this->addFieldMapping('field_uaqs_main_content', 'paragraph_items');
  }

  /**
   * Populate the row with the source bundle field data.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Paragraph items by direct database lookup.
    $hquery = db_select('migrate_map_ltrroutreachheadchunk', 'h')
              ->condition('h.sourceid1', $row->nid)
              ->fields('h', array('destid1'));
    $head = $hquery->execute()->fetchCol(0);
    $bquery = db_select('migrate_map_ltrroutreachbodychunk', 'b')
              ->condition('b.sourceid1', $row->nid)
              ->fields('b', array('destid1'));
    $body = $bquery->execute()->fetchCol(0);
    $fquery = db_select('migrate_map_ltrroutreachfootchunk', 'f')
              ->condition('f.sourceid1', $row->nid)
              ->fields('f', array('destid1'));
    $foot = $fquery->execute()->fetchCol(0);
    $row->paragraph_items = array_merge($head, $body, $foot);
  }

}

/**
 * Defines migration from Node fields into Paragraphs.
 *
 * The migrate_d2d module already provides the class that sets up an existing
 * Drupal database as the migration source, but the DrupalNodeMigration class
 * there assumes a node destination as well, so we can't sub-class it.
 * The Migration module has no direct support for Paragraphs Items
 * (which are use freestanding entities, distinct from nodes), but the
 * Paragraphs module itself defines a Migration api.
 */
abstract class LtrrNodeChunkMigration extends DrupalMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   * @param string $field_name
   *   Name of the field where the Paragraphs attach to a host entity.
   */
  public function __construct(array $arguments, $field_name) {
    parent::__construct($arguments);
    $bundle_dest = $arguments['destination_type'];
    $bundle_src = $arguments['source_type'];

    $this->description = $arguments['description'];

    $this->sourceFields += $this->version->getSourceFields('node', $bundle_src);

    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields, NULL,
      $this->sourceOptions);

    $this->destination = new MigrateDestinationParagraphsItem(
      $bundle_dest,
      array(
        'field_name' => $field_name,
        'text_format' => 'uaqs_textual_content',
      )
    );

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'nid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Source node ID',
          'alias' => 'n',
        ),
      ),
      MigrateDestinationParagraphsItem::getKeySchema()
    );

    if (!$this->newOnly) {
      $this->highwaterField = array(
        'name' => 'changed',
        'alias' => 'n',
        'type' => 'int',
      );
    }
  }

  /**
   * Query for basic node fields from Drupal 7.
   *
   * The migrate_d2d module defines this for Drupal 7 node migrations, but we
   * don't have access to that implementation (not being a subclass of its
   * DrupalNode7Migration).
   *
   * @return object QueryConditionInterface
   *   The source field lookup query.
   */
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
             ->select('node', 'n')
             ->fields('n', array(
               'nid',
               'vid',
               'language',
               'title',
               'uid',
               'status',
               'created',
               'changed',
               'comment',
               'promote',
               'sticky',
               'tnid',
               'translate',
             )
             )
             ->condition('n.type', $this->sourceType)
             ->orderBy($this->newOnly ? 'n.nid' : 'n.changed');
    // Join node_counter for Statistics support.
    if (Database::getConnection('default', $this->sourceConnection)
        ->schema()->tableExists('node_counter')) {
      $query->leftJoin('node_counter', 'nc', 'n.nid=nc.nid');
      $query->addField('nc', 'daycount');
      $query->addField('nc', 'timestamp');
      $query->addField('nc', 'totalcount');
    }
    return $query;
  }

  /**
   * Populate the row with the source bundle field data.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $this->version->getSourceValues($row, $row->nid);
  }

}

/**
 * Defines migration of Outreach Information image field groups to Paragraphs.
 */
abstract class LtrrOutreachImageChunkMigration extends LtrrNodeChunkMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   * @param string $image_src_field
   *   Image source field name (providing the fid for the image file).
   * @param string $caption_src_field
   *   Image source caption field name.
   * @param string $credit_src_field
   *   Image source copyright and attribution field name.
   */
  public function __construct(array $arguments, $image_src_field, $caption_src_field, $credit_src_field) {
    parent::__construct($arguments, 'field_uaqs_main_content');

    // Fields to import to the destination bundle.
    $fields = array(
      $image_src_field => t('Image'),
      $caption_src_field => t('Image caption'),
      $credit_src_field => t('Image attribution and copyright'),
    );

    // Map node fields to paragraphs fields and subfields.
    $this->addFieldMapping('field_uaqs_caption_text', $caption_src_field);
    $this->addFieldMapping('field_uaqs_image_credit', $credit_src_field);

    // Image fields and subfields.
    $image_dst_field = 'field_uaqs_photo';
    $this->addFieldMapping($image_dst_field, $image_src_field);
    $this->addFieldMapping($image_dst_field . ':file_class')
         ->defaultValue('MigrateFileFid');
    $this->addFieldMapping($image_dst_field . ':preserve_files')
         ->defaultValue(TRUE);
    $this->addFieldMapping($image_dst_field . ':alt', $image_src_field . ':alt');
    $this->addFieldMapping($image_dst_field . ':title', $image_src_field . ':title');
    $this->addFieldMapping($image_dst_field . ':width', $image_src_field . ':width');
    $this->addFieldMapping($image_dst_field . ':height', $image_src_field . ':height');
  }

}

/**
 * Defines migration of Outreach Information page head fields into Paragraphs.
 */
class LtrrOutreachHeadChunkMigration extends LtrrOutreachImageChunkMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'field_page_head_image', 'field_page_head_caption', 'field_page_head_credit');
  }

}

/**
 * Defines migration of Outreach Information page foot fields into Paragraphs.
 */
class LtrrOutreachFootChunkMigration extends LtrrOutreachImageChunkMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'field_page_foot_image', 'field_page_foot_caption', 'field_page_foot_credit');
  }

}

/**
 * Defines migration of Outreach Information body fields into Paragraphs.
 */
class LtrrOutreachBodyChunkMigration extends LtrrNodeChunkMigration {

  /**
   * Constructor.
   *
   * @param array $arguments
   *   Arguments for the parent constructor (Migration standard behavior).
   */
  public function __construct(array $arguments) {
    parent::__construct($arguments, 'field_uaqs_main_content');

    // Fields to import to the destination bundle.
    $fields = array(
      'body' => t('Main text'),
    );

    // Map the node body to a to a Paragraphs field.
    $this->addFieldMapping('field_uaqs_html', 'body');

  }

}
